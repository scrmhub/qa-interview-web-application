const path = require('path')
module.exports = {
    entry: "./src/index.jsx",
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, './build/public')
    },
    devtool: "source-map",
    devServer: {
        contentBase: path.resolve(__dirname, './build'),
        port:'3000'
    },
    resolve: {
        extensions: [".js", ".jsx"]
    },
    module: {
        rules: [
            {
                enforce: "pre",
                test: /\.jsx?$/,
                use: ["eslint-loader"],
                exclude: /node_modules/
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
};
