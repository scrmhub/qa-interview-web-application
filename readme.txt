# Google Adword 
React module that shows and update customer's adword configuration
ant-design react hooks

## Runnning
- npm install
- npm start

## Data Structure

const report = {  
  keywords : array,  
  sites : array,  
  browsers : array,  
  incognito : bolean,  
  devices : array  
  advanced : array,  
  waitOnTargetSite : {  
    minutes : number,  
    seconds : number  
  },    
  visitPageWithinSite : bolean,  
  pageVisitTime : {  
    pageNumber : number,  
    minutes : number,  
    seconds : number  
  },    
  waitAfterOperation : {  
    minutes : number,  
    seconds : number    
  },    
  waitTargetSiteNotFound : {  
    times : number,  
    minutes : number  
  },    
  AutomaticResetNumber : number,  
}  