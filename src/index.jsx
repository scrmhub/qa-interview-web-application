import React from 'react';
import ReactDOM from 'react-dom';
import App from './page/Configuration';


const report = {
  keywords:["shop","shop all"],
  sites:["www.a.com","www.b.com"],
  browsers:["Chrome","FireFox","Explorer","Safari","Opera"],
  incognito:true,
  devices:["Device Reset","Vinn Reset","Phone Reset","Mobile Data","Fly Mode"],
  advanced:["Remove Cookies","Change Resolution","Mouse Trach","Data Saving Mode","Random Generate","Analytics Protection","Remove History"],
  waitOnTargetSite:{minutes:20,seconds:10},
  visitPageWithinSite:true,
  pageVisitTime:{pageNumber:2,minutes:20,seconds:20},
  waitAfterOperation:{minutes:5,seconds:10},
  waitTargetSiteNotFound:{times:10,minutes:10},
  automaticResetNumber:1,
}
const onStart = (settings)=>{console.log(settings)}
const onStop = ()=>{console.log('stop')}
const onExport = (settings)=>{console.log(settings)}
ReactDOM.render(<App report={report} {...{onStart,onStop,onExport}} />, document.getElementById('root'));
