import React from 'react';
import PropTypes from 'prop-types';
import Target from './ConfigurationTarget/Configuration.Target';
import { addSite, removeSite } from '../../actions/ConfigurationTargetActions';

const Sites = ({ sites, dispatch }) => {
  return (
    <Target
      list={sites}
      title="Sites"
      icon="tag"
      color="#33cc33"
      description="Enter maximum 5 sites"
      addItem={(value) => { dispatch(addSite(value)) }}
      removeItem={(value) => { dispatch(removeSite(value)) }} />
  )
}

Sites.propTypes = {
  sites: PropTypes.arrayOf(PropTypes.string).isRequired,
  dispatch: PropTypes.func.isRequired
}

export default React.memo(Sites, (prev,next) => {
  return prev.sites === next.sites
 })