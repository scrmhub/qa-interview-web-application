import React from 'react';
import PropTypes from 'prop-types';
import Target from './ConfigurationTarget/Configuration.Target';
import { addKeyword, removeKeyword } from '../../actions/ConfigurationTargetActions';

const KeyWords = ({ keywords, dispatch }) => {
  return (
    <Target
      list={keywords}
      title="Keyword"
      icon="tag"
      description="Enter at least 5 keywords"
      color="#08c"
      addItem={(value) => { dispatch(addKeyword(value)) }}
      removeItem={(value) => { dispatch(removeKeyword(value)) }} />
  )
}
KeyWords.propTypes = {
  keywords: PropTypes.arrayOf(PropTypes.string).isRequired,
  dispatch: PropTypes.func.isRequired
}
export default React.memo(KeyWords, (prev,next) => {
  return prev.keywords === next.keywords
 })