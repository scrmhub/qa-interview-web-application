import React from 'react';
import PropTypes from 'prop-types';
import { Input, Button } from 'antd';
const Search = Input.Search;

const ListInput = ({ title, list, addItem, color }) => {
  const [input, setInput] = React.useState('')
  const handleAdd = (value) => {
    if (value === '') return false;
    if ([...list].includes(value)) return false;
    addItem(value);
    setInput('');
  }
  return (
    <div className="configuration-component-container configuration-target-container" >
      <Search
        className="configuration-target-item"
        value={input}
        onChange={(e) => { setInput(e.target.value) }}
        placeholder={`Enter your ${title} here`}
        enterButton={
          <Button
            disabled={input === ''}
            style={{ backgroundColor: color }}
            size="small" icon="plus-circle" >
            Add
          </Button>
        }
        onSearch={handleAdd}
      />
    </div>
  );
};
ListInput.propTypes = {
  list: PropTypes.arrayOf(PropTypes.string),
  addItem: PropTypes.func,
  color: PropTypes.string,
  title: PropTypes.string.isRequired,
}
export default React.memo(ListInput);