import React from 'react';
import PropTypes from 'prop-types';
import ConfigureHeader from '../Configuration.Header';
import ConfigureDescription from '../Configuration.Description'

import ConfigureList from './Configuration.TargetList';
import ConfigureListInput from './Configuration.TargetInput';

const Target = ({ title, description, icon, list, addItem, removeItem, color }) => {
  return (
    <div>
      <ConfigureHeader {...{title,icon,color}} />
      <ConfigureDescription {...{description, icon,color}} />
      <ConfigureListInput {...{title,list,addItem,color}} />
      <ConfigureList {...{list,removeItem}} />
    </div>
  );
};
Target.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string,
  description: PropTypes.string,
  list: PropTypes.arrayOf(PropTypes.string),
  addItem: PropTypes.func,
  removeItem: PropTypes.func,
  color: PropTypes.string,
}
export default React.memo(Target,(prev,next)=>{
  return prev.list === next.list
});
