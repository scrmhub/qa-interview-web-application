import React from 'react';
import PropTypes from 'prop-types';
import { List, Button } from 'antd';

const ConfigurationList = ({ list, removeItem }) => {
  return (
    <div className="configuration-component-container configuration-list-container">
      <List
        dataSource={list}
        split={false}
        itemLayout="vertical"
        style={{ width: "100%" }}
        size="small"
        renderItem={
          item => (
            <List.Item extra={
              <Button
                style={{ background: "transparent", color: "#FFF" }}
                icon="minus-circle"
                size="small"
                onClick={() => { removeItem(item) }} >
                Clear
              </Button>}>
              {item}
            </List.Item>)} />
    </div>
  );
};
ConfigurationList.propTypes = {
  list: PropTypes.arrayOf(PropTypes.string),
  removeItem: PropTypes.func.isRequired,
}
export default React.memo(ConfigurationList,(prev,next)=>{return prev.list === next.list});
