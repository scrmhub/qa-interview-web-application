import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Checkbox } from 'antd';
import SettingGroup from './SettingGroup'
import { selectBrowsers, selectIncognito } from '../../../actions/ConfigurationSettingsActions';
const BrowsersSetting = ({ browsers, incognito, updateConfiguration }) => {
  const updateBrowser = (value) =>  updateConfiguration(selectBrowsers(value)) ;
  const updateIncognito = (value) => updateConfiguration(selectIncognito(value));
  return (
    <Row
      type="flex">
      <Col
        span={18}
        className="configuration-browsers-container configuration-component-container">
        <SettingGroup
          items={["Chrome", "FireFox", "Explorer", "Safari", "Opera"]}
          value={browsers}
          update={updateBrowser} />
      </Col>
      <Col
        span={6}
        className="configuration-browsers-container configuration-component-container"
        style={{ borderLeft: "2px solid #336699" }}>
        <Row
          type="flex"
          justify="center"
          style={{ width: '100%' }}
          className="configuration-settings-item">
          <Checkbox
            checked={incognito}
            onChange={updateIncognito}
          >
            incognito
          </Checkbox>
        </Row>
      </Col>
    </Row>
  )
}
BrowsersSetting.propTypes = {
  browsers: PropTypes.arrayOf(PropTypes.string),
  incognito: PropTypes.bool,
  updateConfiguration: PropTypes.func.isRequired,
}
export default React.memo(BrowsersSetting, (prev, next) => {
  return prev.browsers === next.browsers && prev.incognito === next.incognito
})