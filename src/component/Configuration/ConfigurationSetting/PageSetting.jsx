import React from 'react';
import { InputNumber, Checkbox } from 'antd';
import PropTypes from 'prop-types';
import { updateWaitOnTargetSite, updatePageVisitTime, updateVisitPageWithinSite, updateWaitAfterOperation, updateWaitTargetSiteNotFound, updateAutomaticResetNumber } from '../../../actions/ConfigurationSettingsActions'
const PageSetting = ({ updateConfiguration, pageSetting }) => {
  const { waitOnTargetSite, visitPageWithinSite, pageVisitTime, waitAfterOperation, waitTargetSiteNotFound, automaticResetNumber } = pageSetting
  return (
    <div className="configuration-pagesetting-container configuration-component-container">
      <div >
        Wait
        <InputNumber
          size="small"
          min={0}
          max={30}
          defaultValue={waitOnTargetSite.minutes}
          onChange={(value) => { updateConfiguration(updateWaitOnTargetSite("minutes", value)) }} />
        minutes
        <InputNumber
          size="small"
          min={0}
          max={30}
          defaultValue={waitOnTargetSite.seconds}
          onChange={(value) => { updateConfiguration(updateWaitOnTargetSite("seconds", value)) }} />
        seconds on the Target website
      </div>
      <div>
        <Checkbox
          checked={visitPageWithinSite}
          onChange={() => { updateConfiguration(updateVisitPageWithinSite()) }} >Visit the page within the site</Checkbox>
      </div>
      <div>
        <InputNumber
          min={0}
          size="small"
          defaultValue={pageVisitTime.pageNumber}
          onChange={(value) => { updateConfiguration(updatePageVisitTime("pageNumber", value)) }} />
        pages
        <InputNumber
          min={0}
          max={30}
          size="small"
          defaultValue={pageVisitTime.minutes}
          onChange={(value) => { updateConfiguration(updatePageVisitTime("minutes", value)) }} />
        minutes
        <InputNumber
          min={0}
          max={30}
          size="small"
          defaultValue={pageVisitTime.seconds}
          onChange={(value) => { updateConfiguration(updatePageVisitTime("seconds", value)) }} />
        visit from to the first to the second
      </div>
      <div>
        After the operation is completed, 
        <InputNumber
          size="small"
          min={0}
          max={30}
          defaultValue={waitAfterOperation.minutes}
          onChange={(value) => { updateConfiguration(updateWaitAfterOperation("minutes", value)) }} />
        minutes
        <InputNumber
          size="small"
          min={0}
          max={30}
          defaultValue={waitAfterOperation.seconds}
          onChange={(value) => { updateConfiguration(updateWaitAfterOperation("seconds", value)) }} />
        seconds to wait.
      </div>
      <div>
        Target site
        <InputNumber
          size="small"
          min={0}
          defaultValue={waitTargetSiteNotFound.times}
          onChange={(value) => { updateConfiguration(updateWaitTargetSiteNotFound("times", value)) }} />
        if not found times
        <InputNumber
          size="small"
          min={0}
          max={30}
          defaultValue={waitTargetSiteNotFound.minutes}
          onChange={(value) => { updateConfiguration(updateWaitTargetSiteNotFound("minutes", value)) }} />
        minutes wait
      </div>
      <div>
        <InputNumber
          size="small"
          min={0}
          defaultValue={automaticResetNumber}
          onChange={(value) => { updateConfiguration(updateAutomaticResetNumber(value)) }}
        />
        automatic reset after operation
      </div>
    </div>
  )
}

PageSetting.propTypes = {
  pageSetting: PropTypes.shape({
    waitOnTargetSite: PropTypes.shape({
      minutes: PropTypes.number,
      seconds: PropTypes.number
    }),
    visitPageWithinSite: PropTypes.bool,
    pageVisitTime: PropTypes.shape({
      pageNumber: PropTypes.number,
      minutes: PropTypes.number,
      seconds: PropTypes.number
    }),
    waitAfterOperation: PropTypes.shape({
      minutes: PropTypes.number,
      seconds: PropTypes.number
    }),
    waitTargetSiteNotFound: PropTypes.shape({
      times: PropTypes.number,
      seconds: PropTypes.number,
      minutes: PropTypes.number
    }),
    automaticResetNumber: PropTypes.number,
  }).isRequired,
  updateConfiguration: PropTypes.func.isRequired
}

export default React.memo(PageSetting, (prev, next) => {
  for (let x in prev.pageSetting) {
    if (prev.pageSetting[x] !== next.pageSetting[x]) { return false }
  }
  return true
})