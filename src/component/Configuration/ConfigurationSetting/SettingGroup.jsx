import React from 'react';
import PropTypes from 'prop-types';
import { Checkbox, Row } from 'antd';
const SettingGroup = ({ items, value, update }) => {
  const onChange = (checkedValues) => {
    update(checkedValues);
  }

  return (
    <Checkbox.Group
      className="configuration-settings-item "
      onChange={onChange}
      value={value}>
      <Row
        type="flex"
        justify="space-between" >
        {items.map((item) =>
          <Checkbox
            key={item}
            value={item}>
            {item}
          </Checkbox>
        )}
      </Row>
    </Checkbox.Group>
  )
}
SettingGroup.propTypes = {
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
  value: PropTypes.arrayOf(PropTypes.string).isRequired,
  update: PropTypes.func.isRequired,
}
export default React.memo(SettingGroup, (prev, next) => {
  return prev.value === next.value && prev.items === next.items
});