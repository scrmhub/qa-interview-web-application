import React from 'react';
import PropTypes from 'prop-types';
import SettingGroup from './SettingGroup'
import { selectDevices } from '../../../actions/ConfigurationSettingsActions';

const DevicesSetting = ({ devices, updateConfiguration }) => {
  const updateDevices = (value) => { updateConfiguration(selectDevices(value)) }
  return (
    <div className="configuration-devices-container configuration-component-container">
      <SettingGroup
        items={["Device Reset", "Vinn Reset", "Phone Reset", "Mobile Data", "Fly Mode"]}
        value={devices}
        update={updateDevices} />
    </div>
  )
}
DevicesSetting.propTypes = {
  devices: PropTypes.arrayOf(PropTypes.string),
  updateConfiguration: PropTypes.func.isRequired
}
export default React.memo(DevicesSetting, (prev, next) => {
  return prev.devices === next.devices;
})