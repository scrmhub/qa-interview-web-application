import React from 'react';
import PropTypes from 'prop-types';
import SettingGroup from './SettingGroup'
import { selectAdvanced } from '../../../actions/ConfigurationSettingsActions';

const AdvancedSetting = ({ advanced, updateConfiguration }) => {
  const updateAdvanced = (value) => { updateConfiguration(selectAdvanced(value)) }
  return (
    <div className="configuration-advanced-container configuration-component-container">
      <SettingGroup
        items={["Remove Cookies", "Change Resolution", "Mouse Trach", "Data Saving Mode", "Random Generate", "Analytics Protection", "Remove History"]}
        value={advanced}
        update={updateAdvanced} />
    </div>
  )
}
AdvancedSetting.propTypes = {
  advanced: PropTypes.arrayOf(PropTypes.string),
  updateConfiguration: PropTypes.func.isRequired
}
export default React.memo(AdvancedSetting, (prev, next) => {
  return prev.advanced === next.advanced
})