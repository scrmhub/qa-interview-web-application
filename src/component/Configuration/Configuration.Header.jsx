import React from 'react';
import PropTypes from 'prop-types';
import { Row, Icon } from 'antd';


const Header = ({ icon, title, color }) => {
  return (
    <Row
      style={{ padding: "12px" }}>
      <Icon
        type={icon}
        style={{ fontSize: '24px', color: color }} />
      <span
        style={{ fontSize: '24px', color: "#ddd", marginLeft: "12px" }}>{title}</span>
    </Row>
  );
};

Header.propTypes = {
  icon: PropTypes.string,
  color: PropTypes.string,
  title: PropTypes.string.isRequired,
};
export default React.memo(Header, (prev, next) => {
  return prev.title === next.title
});

