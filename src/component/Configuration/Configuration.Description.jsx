import React from 'react';
import PropTypes from 'prop-types';
import { Row } from 'antd';


const Description = ({ description }) => {
  return (
    <Row>
      <span style={{ fontSize: '12px', color: "#ddd", marginLeft: "12px" }}>{description}</span>
    </Row>
  );
};

Description.propTypes = {
  description: PropTypes.string.isRequired,
};
export default React.memo(Description, (prev, next) => {
  return prev.description === next.description
});

