// define reducer to update configuration state
export const configurationReducer = (state, action) => {
  switch (action.type) {
    case 'ADD_KEYWORD':
      return {
        ...state,
        keywords: [action.payload]
      }
    case 'REMOVE_KEYWORD':
      return {
        ...state,
        keywords: state.keywords.filter(x => x !== action.payload)
      };
    case 'ADD_SITE':
      return {
        ...state,
        sites: [action.payload, ...state.sites]
      }
    case 'REMOVE_SITE':
      return {
        ...state,
        sites: state.sites.filter(x => x !== action.payload)
      };
    case 'SELECT_BROWSERS':
      return {
        ...state,
        browsers: action.payload,
      }
    case 'SELECT_INCOGNITO':
      return {
        ...state,
        incognito: !state.incognito
      }
    case 'SELECT_DEVICES':
      return {
        ...state,
        devices: action.payload,
      }
    case 'SELECT_ADVANCED':
      return {
        ...state,
        advanced: action.payload,
      }
    case 'UPDATE_WAITONTARGETSITE':
      return {
        ...state,
        waitOnTargetSite: {
          ...state.waitOnTargetSite,
          ...action.payload
        }
      }
    case 'UPDATE_VISITPAGEWITHINSITE':
      return {
        ...state,
        visitPageWithinSite: !state.visitPageWithinSite
      }
    case 'UPDATE_PAGEVISITTIME':
      return {
        ...state,
        pageVisitTime: {
          ...state.pageVisitTime,
          ...action.payload
        }
      }
    case 'UPDATE_WAITAFTEROPERATION':
      return {
        ...state,
        waitAfterOperation: {
          ...state.waitAfterOperation,
          ...action.payload
        }

      }
    case "UPDATE_WAITTAGETSITENOTFOUND":
      return {
        ...state,
        waitTargetSiteNotFound: {
          ...state.waitAfterOperation,
          ...action.payload
        }
      }
    case 'UPDATE_AUTOMATICRESETNUMBER':
      return {
        ...state,
        automaticResetNumber: action.payload,
      }
    default:
      return state
  }
}
