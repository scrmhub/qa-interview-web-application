// defined actions to update configuration state
export const addKeyword = (input) => {
  return {
    type: 'ADD_KEYWORD',
    payload: input
  }
}
export const addSite = (input) => {
  return {
    type: 'ADD_SITE',
    payload: input
  }
}
export const removeKeyword = (input) => {
  return {
    type: 'REMOVE_KEYWORD',
    payload: input
  }
}
export const removeSite = (input) => {
  return {
    type: 'REMOVE_SITE',
    payload: input
  }
}