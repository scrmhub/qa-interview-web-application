// defined actions to update configuration state

export const selectBrowsers = (value) => {
  return {
    type: "SELECT_BROWSERS",
    payload: value
  }
}
export const selectIncognito = () => {
  return {
    type: "SELECT_INCOGNITO",
  }
}
export const selectDevices = (value) => {
  return {
    type: "SELECT_DEVICES",
    payload: value
  }
}
export const selectAdvanced = (value) => {
  return {
    type: "SELECT_ADVANCED",
    payload: value
  }
}
export const updateWaitOnTargetSite = (name, value) => {
  return {
    type: "UPDATE_WAITONTARGETSITE",
    payload: { [name]: value }
  }
}
export const updatePageVisitTime = (name, value) => {
  return {
    type: "UPDATE_PAGEVISITTIME",
    payload: { [name]: value }
  }
}

export const updateVisitPageWithinSite = () => {
  return {
    type: "UPDATE_VISITPAGEWITHINSITE",
  }
}

export const updateWaitAfterOperation = (name, value) => {
  return {
    type: "UPDATE_WAITAFTEROPERATION",
    payload: { [name]: value }
  }
}

export const updateWaitTargetSiteNotFound = (name, value) => {
  return {
    type: "UPDATE_WAITTAGETSITENOTFOUND",
    payload: { [name]: value }
  }
}
export const updateAutomaticResetNumber =  (value) => {
  return {
    type: "UPDATE_AUTOMATICRESETNUMBER",
    payload: value
  }
}