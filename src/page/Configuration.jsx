import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Button } from 'antd';
import Keywords from '../component/Configuration/Configuration.Keywords';
import Sites from '../component/Configuration/Configuration.Sites'
import ConfigureHeader from '../component/Configuration/Configuration.Header';
import BrowsersSetting from '../component/Configuration/ConfigurationSetting/BrowsersSetting';
import DevicesSetting from '../component/Configuration/ConfigurationSetting/DevicesSetting';
import AdvancedSetting from '../component/Configuration/ConfigurationSetting/AdvancedSetting';
import PageSetting from '../component/Configuration/ConfigurationSetting/PageSetting';
import ConfigureDescription from '../component/Configuration/Configuration.Description'
import { configurationReducer } from '../reducers/ConfigurationReducer';
import './configuration.css'


const Configuration = ({ report, onStart, onStop, onExport }) => {
  // memory reducer to prevent unnecessary re-render
  const memoizedReducer = React.useCallback(configurationReducer, []);

  // state for configuration setting, initial value passed as report
  const [configuration, dispatch] = React.useReducer(memoizedReducer, report);

  const { keywords, sites, browsers, incognito, devices, advanced, ...pageSetting } = configuration;
  const updateConfiguration = (action) => { dispatch(action) }
  return (
    <div className="configuration-container">
      <Row type="flex" gutter={16} >
        <Col lg={12} xl={6}>
          <Keywords keywords={keywords} dispatch={updateConfiguration} />
        </Col>
        <Col lg={12} xl={6}>
          <Sites sites={sites} dispatch={updateConfiguration} />
        </Col>
        <Col lg={24} xl={12}>
          <ConfigureHeader
            title="Setings"
            icon="setting"
            color="#ffcc00"
           
          />
          <ConfigureDescription  description="Select the browser settings" />
          <BrowsersSetting
            {...{ browsers, incognito, updateConfiguration }}
          />
          <div className="configuration-setting-container">
            <PageSetting {...{ pageSetting, updateConfiguration }} />
            <DevicesSetting
              {...{ devices, updateConfiguration }}
            />
            <AdvancedSetting
              {...{ advanced, updateConfiguration }}
            />
            <Row type="flex" justify="space-between" >
              <Col span={11}>
                <Button style={{ backgroundColor: "#ffcc00" }} onClick={() => { onExport(configuration) }} >Export Reprt</Button>
              </Col>
              <Col span={6}>
                <Button style={{ backgroundColor: "#08c" }} icon="pause-circle" onClick={() => { onStop() }} >Stop</Button>
              </Col>
              <Col span={6}>
                <Button style={{ backgroundColor: "#33cc33" }} icon="play-circle" onClick={() => { onStart(configuration) }} >Go</Button>
              </Col>
            </Row>

          </div>
        </Col>
      </Row>
    </div>
  );
};
Configuration.propTypes = {
  report: PropTypes.shape({
    keywords: PropTypes.arrayOf(PropTypes.string),
    sites: PropTypes.arrayOf(PropTypes.string),
    browsers: PropTypes.arrayOf(PropTypes.string),
    incognito: PropTypes.bool,
    devices: PropTypes.arrayOf(PropTypes.string),
    advanced: PropTypes.arrayOf(PropTypes.string),
    waitOnTargetSite: PropTypes.shape({
      minutes: PropTypes.number,
      seconds: PropTypes.number
    }),
    visitPageWithinSite: PropTypes.bool,
    pageVisitTime: PropTypes.shape({
      pageNumber: PropTypes.number,
      minutes: PropTypes.number,
      seconds: PropTypes.number
    }),
    waitAfterOperation: PropTypes.shape({
      minutes: PropTypes.number,
      seconds: PropTypes.number
    }),
    waitTargetSiteNotFound: PropTypes.shape({
      times: PropTypes.number,
      minutes: PropTypes.number
    }),
    automaticResetNumber: PropTypes.number,
  }),
  onStart: PropTypes.func.isRequired,
  onStop: PropTypes.func.isRequired,
  onExport: PropTypes.func.isRequired
}
export default React.memo(Configuration);
