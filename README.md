# Conduct effective QA interviews using this web application as a backdrop

This simple web application is an ideal 'product' for interviewing QA.  We recommend reserving one round (between 30-60 minutes) of the interview for testing this application.

![Alt text](./asset/screen_shot_of_test_application.png "Title")

## Features of the 'Fizz buzz for QA' application

### The app has been seeded with a variety of bugs

* a major functional bug
* a data limit bug
* a usability bug
* a typo
* a bug to test if the QA is reading the page content
* a slightly weird wording
* a cross-browser bug

### The app also has several workflows which will work well and that the QA being interviewed should check

* E.g.: Entering an integer followed by a string followed by an integer (does the form validation clear?)

### You can use this app to conduct more technical interviews too. You can ask the candidate to

* write a locator (CSS selector/XPath) for the red form validation styling
* find the console message printed
* write a Selenium script to test that the factorial of 5 is 120
* figure out the API call being made along with the headers and parameters sent
* write a bug report
* document a test case

## Setup

Follow this setup to use a local copy of this application.

1. `yarn / npm install`

## How to run the QA interview application

To start the application, run `yarn start / npm run start` and visit `localhost:3000` in your browser.

## What the application does

The application creates an report of websites' activities. We have exactly one page with:

* two text boxes
* few number box
* three buttons
* few tick boxes
* three section titles
